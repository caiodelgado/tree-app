﻿
(function () {
  document.addEventListener('deviceready', onDeviceReady.bind(this), false);
  function onDeviceReady() {

    var reloadInvesters = function () {
      require(["database"], function (db) {

        var investers = db.getInvesters();
        $("#invester").empty();
        $.each(investers, function (i, item) {
          $('#invester').append($('<option>', {
            value: item.Id,
            text : db.getCompanyById(item.MainCompany).Name + ' => ' + db.getCompanyById(item.InvesterCompany).Name
          }));
        });
      });
    };

    reloadInvesters();

    $("#btnRemoveInvester").click(function (evt) {
      evt.preventDefault();

      var selectedInvester = $('#invester option:selected').val();

      require(["database"], function (db) {
        var invester = db.getInvesterById(selectedInvester);
        db.deleteInvester(invester);
        reloadInvesters();
      });

    });

  }

})();