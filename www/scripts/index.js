﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in cordova-simulate or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.

(function () {
  "use strict";

  document.addEventListener('deviceready', onDeviceReady.bind(this), false);

  function onDeviceReady() {
    $("#form-busca").submit(onSearchButtonClicked);
    require(["database"], function (db) {
      db.openDatabase();
      var companies = db.getCompanies();
      populateDropdown(companies);
    });


    // TODO: Cordova has been loaded. Perform any initialization that requires Cordova here.
    google.charts.load('current', { packages: ["orgchart"] });
    //google.charts.setOnLoadCallback(drawChart);

    function drawChart(investers) {
      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Invester');
      data.addColumn('string', 'Company');
      data.addColumn('string', 'ToolTip');

      // For each orgchart box, provide the name, manager, and tooltip to show.
      data.addRows([
        [{ v: 'Mike', f: 'Mike<div style="color:red; font-style:italic">President</div>' },
          '', 'The President'],
        [{ v: 'Jim', f: 'Jim<div style="color:red; font-style:italic">Vice President</div>' },
          'Mike', 'VP'],
        ['Alice', 'Mike', ''],
        ['Bob', 'Jim', 'Bob Sponge'],
        ['Carol', 'Bob', '']
      ]);

      // Create the chart.
      var chart = new google.visualization.OrgChart(document.getElementById("google-chart"));
      // Draw the chart, setting the allowHtml option to true for the tooltips.
      chart.draw(data, { allowHtml: true });
    }
  };

  function populateDropdown(companies) {
    var empresa = $("#empresa");

    companies.forEach(function (company) {
      var option = $("<option>");
      option.text(company.Name);
      option.val(company.Id);

      empresa.append(option)
    })
  };

  function onSearchButtonClicked(evt) {
    evt.preventDefault();
    var selectedCompanyId = parseInt($("#empresa").val());

    require(["database"], function (db) {
      var companies = db.getCompanies();

      var selectedCompany = companies.find(function (company) {
        return company.Id == selectedCompanyId;
      });

      var investers = db.getInvesters();
      var root = { InvesterCompany: selectedCompany.Id, MainCompany: 0, Shares: selectedCompany.Shares };
      var treeData = generateTreeData(root, db, investers);
      drawTree(treeData);      
    });


  };


  function generateTreeData(invester, db, investers) {
    var data = [convertToTreeNode(db, invester)];

    var children = investers.filter(function (element, index, array) {
      return element.MainCompany == invester.InvesterCompany;
    })

    if (!children.length)
      return data;

    children.forEach(function (child) {
      var innerTreeData = generateTreeData(child, db, investers);
      data = data.concat(innerTreeData);
    });

    return data;
  }

  function convertToTreeNode(db, invester) {

    var investerCompany = db.getCompanyById(invester.InvesterCompany);
    var mainId = (invester.MainCompany || "").toString();
    var nodeContent = `<h4>${investerCompany.Name}</h4><p>${invester.Shares}</p>`
    var treeNode = [{ v: investerCompany.Id.toString(), f: nodeContent }, mainId, invester.Shares.toString()];

    return treeNode;
  }

  function drawTree(treeData) {
    var dataTable = new google.visualization.DataTable();
    dataTable.addColumn('string', 'Invester');
    dataTable.addColumn('string', 'Company');
    dataTable.addColumn('string', 'ToolTip');

    dataTable.addRows(treeData);

    // Create the chart.
    var chart = new google.visualization.OrgChart(document.getElementById("google-chart"));
    // Draw the chart, setting the allowHtml option to true for the tooltips.
    chart.draw(dataTable, { allowHtml: true });
  }

})();