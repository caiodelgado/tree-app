﻿
(function () {
    document.addEventListener('deviceready', onDeviceReady.bind(this), false);
    function onDeviceReady() {

        var reloadCompanies = function () {
            require(["database"], function (db) {

                var companies = db.getCompanies();
                $("#empresaPrincipal").empty();
                $.each(companies, function (i, item) {
                    $('#empresaPrincipal').append($('<option>', {
                        value: item.Id,
                        text : item.Name
                    }));
                });
            });
        };

        reloadCompanies();

        $("#btnRemoveEmpresa").click(function (evt) {
            evt.preventDefault();

            var selectedCompany = $('#empresaPrincipal option:selected').val();

            require(["database"], function (db) {
                var company = db.getCompanyById(selectedCompany);
                db.deleteCompany(company);
                reloadCompanies();
            });

        });

    }

})();