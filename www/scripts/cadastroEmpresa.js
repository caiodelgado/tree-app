﻿
(function () {
    document.addEventListener('deviceready', onDeviceReady.bind(this), false);
    function onDeviceReady() {

        $("#form-cadastro").submit(function (evt) {
            evt.preventDefault();

            var name = $("#nome").val();
            $("#nome").val(null)
            var shares = parseFloat($("#acoes").val());
            $("#acoes").val(null)

            require(["database"], function (db) {
                var company = {
                    Id: db.identity(),
                    Name: name,
                    Shares: shares,
                    CurrentShares: shares
                };

                db.insertCompany(company);

                alert("Cadastro de Empresa realizado com sucesso!")
            });

        });
    }

})();