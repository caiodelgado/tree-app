﻿
(function () {
    document.addEventListener('deviceready', onDeviceReady.bind(this), false);
    function onDeviceReady() {

        require(["database"], function (db) {

            var companies = db.getCompanies();

            $.each(companies, function (i, item) {
                $('#empresaPrincipal').append($('<option>', {
                    value: item.Id,
                    text : item.Name
                }));

                $('#empresaInvestidora').append($('<option>', {
                    value: item.Id,
                    text : item.Name
                }));
            });

            loadShares();
        });

        var loadShares = function () {
          var selectedCompany = $('#empresaPrincipal option:selected').val();

          require(["database"], function (db) {
            var company = db.getCompanyById(selectedCompany);
            $('#shares').text(company.Shares);
            $('#currentShares').text(company.CurrentShares);
          });
        }

        $('#empresaPrincipal').change(function (evt) {
            evt.preventDefault();
            loadShares();
        });

        $("#form-cadastro").submit(function (evt) {
            evt.preventDefault();

            var mainCompany = $("#empresaPrincipal").val();
            var investerCompany = $("#empresaInvestidora").val();
            var shares = $("#acoes").val();
            var currentShares = $('#currentShares').text();
            console.log(currentShares)
            if (shares <= 0)
                alert("Investimento deve ser superior a 0.")
            else if (mainCompany === investerCompany) {
              alert("Uma empresa não pode investir nela mesmo.")
            } else if (parseInt(shares) > parseInt(currentShares)) {
              alert("Valor de investimento superior ao disponível, verifique.")
            } else {
                require(["database"], function (db) {
                    var invester = {
                        Id: db.identity(),
                        MainCompany: mainCompany,
                        InvesterCompany: investerCompany,
                        Shares: shares
                    };

                    db.insertInvester(invester);

                    alert("Cadastro de Investimento realizado com sucesso!")
                });
            }

        });
    }

})();