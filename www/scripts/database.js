﻿
var db = null;

define("database", function () {
    console.log("Function : openDatabase");

    return {
        openDatabase: function () {
          if (!this.select("Companies"))
            this.save("Companies", []);

          if (!this.select("Investers"))
            this.save("Investers", []);

          if (!this.select("IdentityId"))
            this.save("IdentityId", '1');
        },

        insertInvester: function (invester) {
            var investers = this.select("Investers") || [];
            var mainCompany = this.getCompanyById(invester.MainCompany);
            this.deleteCompany(mainCompany);
            mainCompany.CurrentShares -= invester.Shares;
            this.insertCompany(mainCompany);
            investers.push(invester);
            this.save("Investers", investers);
        },

        getInvesters: function () {
          var investers = this.select("Investers") || [];
          return investers;
        },

        deleteInvester: function (invester) {
          var investers = this.select("Investers") || [];
          var position = investers.findIndex(function (x) {
            return invester.Id == x.Id
          });
          var mainCompany = this.getCompanyById(invester.MainCompany);
          this.deleteCompany(mainCompany);
          mainCompany.CurrentShares = parseInt(mainCompany.CurrentShares) + parseInt(invester.Shares);
          this.insertCompany(mainCompany);
          investers.splice(position, 1);
          this.save("Investers", investers);
        },

        getInvesterById: function (id) {
          var investers = this.select("Investers") || [];
          return investers.filter(function (x) {
            return x.Id == id
          })[0];
        },

        insertCompany: function (company) {
          var companies = this.select("Companies") || [];
          companies.push(company);
          this.save("Companies", companies);
        },

        getCompanies: function (callback) {
          var companies = this.select("Companies") || [];
          return companies;
        },

        getCompanyById: function (id) {
          var companies = this.select("Companies") || [];
          return companies.filter(function (x) {
              return x.Id == id
          })[0];
        },

        deleteCompany: function (company) {
          var companies = this.select("Companies") || [];
          var position = companies.findIndex(function (x) {
            return company.Id == x.Id
          });
          companies.splice(position, 1);
          this.save("Companies", companies);
        },

        select: function (tableName) {
          var serializedArray = window.localStorage.getItem(tableName);
          var tableArray = JSON.parse(serializedArray);
          return tableArray;
        },

        save: function (tableName, tableArray) {
          var serializedArray = JSON.stringify(tableArray);
          window.localStorage.setItem(tableName, serializedArray);
        },

        identity: function () {
            var id = parseInt(window.localStorage.getItem('IdentityId') || 0) + 1;
            window.localStorage.setItem('IdentityId', id);
            return id;
        }
    }
});